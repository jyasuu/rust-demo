use ferris_says::say;
use rust_demo::{DataStoreError, Person, Struct}; // from the previous step
use std::io::{self, stdout, BufWriter};
use log::{info, warn};
use tokio::time::{sleep, Duration};
use serde::{Deserialize, Serialize};
use std::collections::VecDeque;
use std::collections::HashMap;


#[tokio::main]
async fn main()-> Result<(), DataStoreError> {
    println!("Hello, world!");
    let stdout = stdout();
    let message = String::from("Hello fellow Rustaceans!");
    let width = message.chars().count();

    let mut writer = BufWriter::new(stdout.lock());
    say(&message, width, &mut writer).unwrap();

    env_logger::init();
    info!("starting up");
    warn!("oops, nothing implemented!");


    {
        let mut s = String::from("Hello");
        s.push_str(", world!"); // Appends a string slice
        s.push('!');            // Appends a single character

    }
    {
        let mut s = String::from("Hello");
        s.push_str(", world!"); // Appends a string slice
        s.push('!');            // Appends a single character

    }
    {
        let s1 = String::from("Hello");
        let s2 = String::from("World");
        let s3 = s1 + " " + &s2; // s1 is moved, cannot be used anymore

    }
    {
        let name = "Rust";
        let s = format!("Hello, {}!", name); // Similar to println!, but returns a String

    }
    {
        for c in "Hello".chars() {
            println!("{}", c);
        }
        
    }
    {
        let len = "Hello".len(); // Length in bytes, not characters

    }
    {
        let s = &"Hello"[0..4]; // Slicing a string (returns a string slice)

    }

    {
        let v: Vec<i32> = Vec::new(); // Empty vector
        let v = vec![1, 2, 3];        // Using the vec! macro

    }
    {
        let mut v = Vec::new();
        v.push(5);
        v.push(6);
        v.push(7);

    }
    {
        let v = vec![1, 2, 3, 4, 5];
        let third = &v[2]; // Access by index

    }
    {
        let v = vec![1, 2, 3, 4, 5];
        for i in &v {
            println!("{}", i);
        }

    }
    {
        let mut v = vec![1, 2, 3];
        v.pop(); // Removes the last element

    }
    {
        let mut v = vec![1, 2, 3];
        let len = v.len(); // Number of elements in the vector

    }
    {

        let mut vd: VecDeque<i32> = VecDeque::new(); // Empty VecDeque
        vd.push_back(1); // Add to the back
        vd.push_front(0); // Add to the front
        vd.pop_back();  // Removes the last element
        vd.pop_front(); // Removes the first element
        let first = vd.front();  // Returns Option<&T> for the first element
        let last = vd.back();    // Returns Option<&T> for the last element
        for x in &vd {
            println!("{}", x);
        }
        let len = vd.len(); // Number of elements in the VecDeque


    }
    {

        let mut scores = HashMap::new(); // Empty HashMap
        scores.insert(String::from("Blue"), 10);
        scores.insert(String::from("Yellow"), 50);
        let score = scores.get("Blue"); // Returns Option<&V>
        for (key, value) in &scores {
            println!("{}: {}", key, value);
        }
        scores.insert(String::from("Blue"), 25); // Overwrites the existing value
        scores.remove("Yellow"); // Removes the key-value pair
        if scores.contains_key("Blue") {
            println!("Blue team is in the game!");
        }
                        
    }

    {
        let some_value = Some(5);     // An Option containing a value
        let no_value: Option<i32> = None; // An Option with no value
        let x = Some(2);
        let y = x.unwrap(); // Returns the contained value, panics if None
        let some_number = Some(7);

        match some_number {
            Some(value) => println!("The value is: {}", value),
            None => println!("No value"),
        }
        let some_number = Some(7);

        if let Some(value) = some_number {
            println!("The value is: {}", value);
        } else {
            println!("No value");
        }
        let x = Some(5);
        let y = x.unwrap_or(10); // Returns 5 if x is Some(5), otherwise returns 10

        let z = None.unwrap_or_else(|| {
            // Compute the default value
            42
        });
        let some_number = Some(5);
        let new_number = some_number.map(|x| x + 1); // new_number is Some(6)

    }

    {
        let v = vec![1, 2, 3];
        let iter = v.iter(); // Creates an iterator over references to elements
        let v = vec![1, 2, 3];
        for val in v.iter() {
            println!("{}", val);
        }
        let v = vec![1, 2, 3];
        let doubled: Vec<i32> = v.iter().map(|x| x * 2).collect();
        let mut iter = vec![1, 2, 3].into_iter();
        assert_eq!(iter.next(), Some(1)); // Returns the next element
        assert_eq!(iter.next(), Some(2));
        assert_eq!(iter.next(), Some(3));
        assert_eq!(iter.next(), None);    // No more elements
        let numbers: Vec<i32> = vec![1, 2, 3, 4, 5];
    
        // Filter out even numbers
        let evens :Vec<_> = numbers.iter().filter(|&&x| x % 2 == 0).collect();
    
        // Double each number
        let doubled: Vec<i32> = numbers.iter().map(|x| x * 2).collect();
    
        // Sum all the numbers
        let sum: i32 = numbers.iter().fold(0, |acc, &x| acc + x);
        let numbers = vec![1, 2, 3, 4, 5];
        let result: Vec<i32> = numbers.iter()
            .filter(|&&x| x % 2 == 0)  // Filter even numbers
            .map(|x| x * 2)            // Double them
            .collect();                // Collect into a vector
        
    }

    {
        
        let penguin_data = "\
        common name,length (cm)
        Little penguin,33
        Yellow-eyed penguin,65
        Fiordland penguin,60
        Invalid,data
        ";

        let records = penguin_data.lines();

        for (i, record) in records.enumerate() {
            if i == 0 || record.trim().len() == 0 {
            continue;
            }

            let fields: Vec<_> = record
            .split(',')
            .map(|field| field.trim())
            .collect();
            if cfg!(debug_assertions) {
            eprintln!("debug: {:?} -> {:?}",
                    record, fields);
            }

            let name = fields[0];

            if let Ok(length) = fields[1].parse::<f32>() {
                println!("{}, {}cm", name, length);
            }
        }
    }

    rust_demo::hosting::add_to_waitlist();

    {
        let a = 10;
        let b: i32 = 20;
        let mut c = 30i32;
        let d = 30_i32;
        let e = add(add(a, b), add(c, d));
    
        println!("( a + b ) + ( c + d ) = {}", e);
    }

    {
        // mut able modify
        let mut x = 5;
        println!("The value of x is: {}", x);
        x = 6;
        println!("The value of x is: {}", x);
    }
    {
        // prefix _ naming ignore unused warning
        let _x = 5;
        let y = 10;
    }

    {
            
        let (a, mut b): (bool,bool) = (true, false);
        println!("a = {:?}, b = {:?}", a, b);

        assert_eq!(a, !b);
        b = true;
        assert_eq!(a, b);
    }

    {
        
        let (a, b, c, d, e);

        (a, b) = (1, 2);
        
        [c, .., d, _] = [1, 2, 3, 4, 5];
        Struct { e, .. } = Struct { e: 5 };

        assert_eq!([1, 2, 1, 4, 5], [a, b, c, d, e]);
    }

    {
        
        let x = 5;
        let x = x + 1;

        {
            let x = x * 2;
            println!("The value of x in the inner scope is: {}", x);
        }

        println!("The value of x is: {}", x);
    }

    {

        println!("Hello, world!");
        sleep(Duration::from_secs(1)).await;
        println!("Goodbye, world!");
    }

    {

        let json = r#"
        {
            "name": "John Doe",
            "age": 43
        }"#;

        let p: Person = serde_json::from_str(json).unwrap();
        println!("{:?}", p);
    }

    {

        match fetch_url("https://api.github.com/repos/rust-lang/rust").await {
            Ok(content) => println!("Response: {}", content),
            Err(e) => println!("Error: {}", e),
        }
        // let response = reqwest::get("https://api.github.com/repos/rust-lang/rust")
        // .await?
        // .text()
        // .await?;

        // println!("Response: {}", response);
    }

    Err(DataStoreError::Redaction("Something went wrong".into()))

}



async fn fetch_url(url: &str) -> Result<String, DataStoreError> {
    let client = reqwest::Client::new();
    let response = client.get(url)
        .header(reqwest::header::USER_AGENT, "My Rust App")
        .send().await.map_err(DataStoreError::HttpRequest)?;
    let body = response.text().await?;
    Ok(body)
}





fn add_one(num: i32) -> i32 {
    info!("add_one called with {}", num);
    num + 1
}

fn add(i: i32, j: i32) -> i32 {
    i + j
}


#[cfg(test)]
mod tests {
    use super::*;
    use log::info;

    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn it_adds_one() {
        init();

        info!("can log from the test too");
        assert_eq!(3, add_one(2));
    }

    #[test]
    fn it_handles_negative_numbers() {
        init();

        info!("logging from another test");
        assert_eq!(-7, add_one(-8));
    }
}

