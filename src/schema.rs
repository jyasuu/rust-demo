// @generated automatically by Diesel CLI.

diesel::table! {
    user (id) {
        id -> Int4,
        #[max_length = 50]
        username -> Varchar,
        #[max_length = 100]
        email -> Varchar,
        #[max_length = 100]
        password -> Varchar,
        created_at -> Timestamp,
    }
}
