


#[derive(thiserror::Error, Debug)]
pub enum DataStoreError {
    #[error("HttpRequest disconnected")]
    HttpRequest(#[from] reqwest::Error),

    #[error("data store disconnected")]
    Disconnect(#[from] std::io::Error),

    #[error("the data for key `{0}` is not available")]
    Redaction(String),

    #[error("invalid header (expected {expected:?}, found {found:?})")]
    InvalidHeader {
        expected: String,
        found: String,
    },

    #[error("unknown data store error")]
    Unknown,
}
