use std::collections::BinaryHeap;
use std::cmp::Reverse;

struct Solution;

impl Solution {
    pub fn min_meeting_rooms(intervals: Vec<Vec<i32>>) -> i32 {
        if intervals.is_empty() {
            return 0;
        }

        // Sort intervals by start time
        let mut intervals = intervals;
        intervals.sort_unstable_by_key(|interval| interval[0]);

        // Min heap to keep track of end times
        let mut end_times = BinaryHeap::new();

        // Add the end time of the first meeting
        end_times.push(Reverse(intervals[0][1]));

        for interval in intervals.iter().skip(1) {
            let start = interval[0];
            let end = interval[1];

            // If the earliest ending meeting has ended, remove it
            if let Some(Reverse(earliest_end)) = end_times.peek() {
                if *earliest_end <= start {
                    end_times.pop();
                }
            }

            // Add the current meeting's end time
            end_times.push(Reverse(end));
        }

        // The size of the heap is the number of rooms needed
        end_times.len() as i32
    }

    // Helper function to print the scheduling process
    pub fn print_scheduling_process(intervals: &Vec<Vec<i32>>) {
        let mut intervals = intervals.clone();
        intervals.sort_unstable_by_key(|interval| interval[0]);

        let mut end_times = BinaryHeap::new();
        let mut room_count = 0;

        println!("Scheduling process:");
        for (i, interval) in intervals.iter().enumerate() {
            let start = interval[0];
            let end = interval[1];

            while let Some(Reverse(earliest_end)) = end_times.pop() {
                if earliest_end <= start {
                    println!("  Room freed at time {}", earliest_end);
                } else {
                    end_times.push(Reverse(earliest_end));
                    break;
                }
            }

            end_times.push(Reverse(end));
            if end_times.len() > room_count {
                room_count = end_times.len();
                println!("  New room allocated");
            }
            println!("Meeting {} scheduled: ({}, {}). Current rooms: {}", i + 1, start, end, end_times.len());
        }
        println!("Final number of rooms required: {}", room_count);
    }
}

fn main() {
    let test_cases = vec![
        vec![vec![0, 30], vec![5, 10], vec![15, 20]],
        vec![vec![7, 10], vec![2, 4]],
        vec![vec![1, 5], vec![8, 9], vec![8, 15]],
    ];

    for (i, intervals) in test_cases.iter().enumerate() {
        println!("Test case {}:", i + 1);
        println!("Intervals: {:?}", intervals);
        let min_rooms = Solution::min_meeting_rooms(intervals.to_vec());
        println!("Minimum number of meeting rooms required: {}", min_rooms);
        Solution::print_scheduling_process(intervals);
        println!();
    }
}