use std::collections::BinaryHeap;
use std::cmp::Reverse;

fn max_subsequence_score(nums1: Vec<i32>, nums2: Vec<i32>, k: usize) -> i64 {
    // Pair elements and sort them based on nums2 in descending order
    let mut pairs: Vec<(i32, i32)> = nums1.into_iter().zip(nums2.into_iter()).collect();
    pairs.sort_by(|a, b| b.1.cmp(&a.1));
    
    
    let mut heap = BinaryHeap::new();
    let mut sum = 0_i64;
    let mut max_score = 0_i64;
    
    // Process pairs
    for (val1, val2) in pairs {
        println!("pairs is {},{}", val1,val2);
        heap.push(Reverse(val1));
        sum += val1 as i64;
        
        if heap.len() > k {
            if let Some(Reverse(min_val)) = heap.pop() {
                sum -= min_val as i64;
            }
        }
        
        if heap.len() == k {
            println!("sum is {},{}", sum,val2);
            max_score = max_score.max(sum * val2 as i64);
        }
    }
    
    max_score
}

fn main() {
    let nums1 = vec![1, 3, 3, 4, 2];
    let nums2 = vec![2, 1, 3, 2, 1];
    let k = 3;
    let result = max_subsequence_score(nums1, nums2, k);
    println!("The maximum subsequence score is {}", result); // Output: 15

    let nums1 = vec![5, 2, 3, 1, 4];
    let nums2 = vec![3, 2, 1, 4, 5];
    let k = 2;
    let result = max_subsequence_score(nums1, nums2, k);
    println!("The maximum subsequence score is {}", result); // Output: 25
}
