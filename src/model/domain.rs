use serde::{Deserialize, Serialize};


pub struct Struct {
    pub e: i32
}



#[derive(Serialize, Deserialize, Debug)]
pub struct Person {
    name: String,
    age: u8,
}
