mod front_of_house;
mod error;
mod model;

pub use crate::front_of_house::hosting;

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
}


pub use error::DataStoreError;
pub use model::*;